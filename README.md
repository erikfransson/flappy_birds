## AI learns to play flappy bird

Some testing for the problem of teaching an AI to play flappy bird.
Uses pygame and pybrain

### Run
To play yourself (human), run
    
    python src/run_human_play.py

To learn the AI to play, run

    python src/evolution_simulation.py

other `run_*.py` scripts are quickly put together for e.g. visualzing network or trying ensemble birds.

### Demo
<img src="images/flappy.png" alt="drawing" width="750"/>

<img src="images/flappy_demo.gif" alt="drawing" width="750"/>



### Todo
* NNVisualizer is horrible