import os
import sys
import pygame
import random
import pickle
import numpy as np
from flappy_birds import Engine
from nn_bird import NNBird


class EvolutionaryFlappyBirds:

    # graphics
    size_x = 1600
    size_y = 900
    FPS = 90

    # population
    x_elites = 0.2
    x_children = 0.6
    x_newblood = 0.2

    # mutations
    mutation_prob = 0.1    # probability of a parameter being changed
    mutation_std = 1.0     # std for changing parameters

    # colors
    color_elite = (212, 175, 55)
    color_children = (40, 70, 240)
    color_newblood = (200, 50, 100)
    colors = {'elites': color_elite, 'children': color_children, 'new_blood': color_newblood}

    def __init__(self, population_size, mode='easy'):
        self.population_size = population_size
        self.mode = mode
        self.setup_initial_population()

        # setup screen and clock
        self.screen = pygame.display.set_mode((self.size_x, self.size_y), pygame.SRCALPHA, 32)
        self.clock = pygame.time.Clock()

    def setup_initial_population(self):
        """ initial population is only new blood """
        self.n_elites = int(self.x_elites * self.population_size)
        self.n_children = int(self.x_children * self.population_size)
        self.n_newblood = int(self.x_newblood * self.population_size)
        assert self.n_elites + self.n_children + self.n_newblood == self.population_size

        self.population = {n: [] for n in ['new_blood', 'children', 'elites']}
        for _ in range(self.population_size):
            self.population['new_blood'].append(NNBird())

    @property
    def population_names(self):
        return list(self.population.keys())

    @property
    def population_list(self):
        p = self.population
        return p['new_blood'] + p['children'] + p['elites']

    def run(self, n_generations=1000, seed=None, savedir=None, birds_fname=None):
        """ setting seed makes each game for each generation identical,
        else pipes are randomized for each generation """

        if savedir is not None:
            self.frame_counter = 0

        # run evolution
        for gen in range(n_generations):
            self.generation = gen

            # reset and color birds
            for name, birds in self.population.items():
                color = self.colors[name]
                for bird in birds:
                    bird.color = color
                    bird.reset()

            self._run_game(seed=seed, savedir=savedir)
            self.generate_next_generation()

            # save elites to file
            if birds_fname is not None:
                fobj = open(birds_fname, 'wb')
                data = self.population['elites']
                pickle.dump(data, fobj)

    def _run_game(self, seed=None, savedir=None):
        game = Engine(self.size_x, self.size_y, self.population_list, mode=self.mode, seed=seed)
        while game.has_alive_birds:

            # events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()

            game.update()
            game.draw(self.screen)
            self._draw_status()
            pygame.display.flip()
            self.clock.tick(self.FPS)

            if savedir is not None:
                if self.frame_counter < 60000:
                    self.frame_counter += 1
                    fname = os.path.join(savedir, 'state-{}.png'.format(self.frame_counter))
                    pygame.image.save(self.screen, fname)

    def generate_next_generation(self):

        # sort by score, if multiple have same time_alive, sort by y-dist to pipe
        pop = sorted(self.population_list, key=lambda x: (-x.time_alive, x.y_dist_to_pipe))
        scores = np.array([bird.time_alive/1000 for bird in pop])

        # keep the elites
        self.population['elites'] = [pop[i] for i in range(self.n_elites)]

        # generate new blood
        new_blood = []
        for _ in range(self.n_newblood):
            new_blood.append(NNBird())
        self.population['new_blood'] = new_blood

        # let elites breed with probablity related to their score
        exp_scores = np.exp(scores[:self.n_elites])
        probs = exp_scores / sum(exp_scores)
        children = []
        for _ in range(self.n_children):
            parent = np.random.choice(self.population['elites'], p=probs)
            child = self.spawn_child(parent)
            children.append(child)
        self.population['children'] = children

    def spawn_child(self, bird):
        params = bird.net.params.copy()
        mutate_array(params, self.mutation_prob, self.mutation_std)
        child = NNBird()
        child.net._setParameters(params)
        return child

    def _draw_status(self):
        # parameters
        x = 20
        y = 30
        dy = 40
        fs = 40

        # draw generation count and mode
        y = y + 1.2*dy
        font = pygame.font.SysFont('timesnewroman', fs, bold=True)
        text = font.render('Generation {:3d}'.format(self.generation), False, (0, 0, 0))
        self.screen.blit(text, (x, y))
        y = y + 1.8*dy

        # draw explanation of colors
        bird = self.population_list[0]
        r = bird.radius
        t = bird.thickness
        for name in self.population_names:
            color = self.colors[name]
            name = name.replace('_', ' ')
            center = (int(x+r), int(y+r))
            pygame.draw.circle(self.screen, color, center, r, t)
            text = font.render(name.title(), False, color)
            self.screen.blit(text, (x+3*r, y-3))
            y = y + 1.4*dy

        # draw alive counts
        y = y + 0.4*dy
        for name in self.population_names:
            n_alive = sum(1 for bird in self.population[name] if bird.is_alive)
            n_total = len(self.population[name])
            color = self.colors[name]
            s = _get_alive_string(n_alive, n_total)
            text = font.render(s, True, color)
            self.screen.blit(text, (x, y))
            y = y + dy

    def quit(self):
        pygame.quit()
        sys.exit()

# Helper functions
# ------------------


def mutate_array(array, mutation_prob, mutation_std):

    seed = random.randint(0, 1000000000)
    rs = np.random.RandomState(seed)

    # change weights
    mutation_mask = rs.random(array.shape) < mutation_prob
    while np.sum(mutation_mask) == 0:
        mutation_mask = rs.random(array.shape) < mutation_prob
    mutation = mutation_mask * rs.normal(0, mutation_std, array.shape)
    array += mutation


def _get_alive_string(n_alive, n_total):
    # s = 'Alive  {:4d} / {:3d}'.format(n_alive, n_total)
    n_digits = len(str(n_alive))
    n_spaces = (3 - n_digits) * 2
    s = 'Alive ' + ' ' * n_spaces + str(n_alive) + ' / ' + str(n_total)
    return s


if __name__ == '__main__':

    # evolution parameters
    pop_size = 200
    seed = None

    # setup
    pygame.init()
    np.random.seed(420)
    evo = EvolutionaryFlappyBirds(population_size=pop_size, mode='easy')
    evo.run(seed=seed, n_generations=1000)
