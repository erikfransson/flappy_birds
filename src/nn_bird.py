import numpy as np
from flappy_birds import Bird
from pybrain.structure import SigmoidLayer, TanhLayer
from pybrain.tools.shortcuts import buildNetwork


class NNBird(Bird):

    layout = (6, 4, 4, 1)

    def __init__(self, start_y=450):
        super().__init__(start_y)
        self.net = buildNetwork(*self.layout, hiddenclass=TanhLayer, outclass=SigmoidLayer)
        self.vy = 0

    def update(self, dt, closest_pipe):
        self.time_alive += dt

        # activate net
        self.closest_pipe = closest_pipe
        inputs = self.get_input_array()
        output = self.net.activate(inputs)

        # jump or not
        if output > 0.5:
            self.vy += self.jump_burst

        # equation of motion
        self.vy += self.gravity*dt
        self.y += self.vy * dt
        self.rect.y = self.y

    def get_input_array(self):
        length_scale = 1000

        # distances
        x_dist1 = self.x - self.closest_pipe.x
        x_dist2 = self.x - (self.closest_pipe.x + self.closest_pipe.width)
        y_dist1 = self.y - self.closest_pipe.y1
        y_dist2 = self.y - self.closest_pipe.y2

        # features
        f1 = self.vy
        f2 = self.y / length_scale
        f3 = (length_scale - x_dist1) / length_scale
        f4 = (length_scale - x_dist2) / length_scale
        f5 = (length_scale - y_dist1) / length_scale
        f6 = (length_scale - y_dist2) / length_scale

        inputs = np.array([f1, f2, f3, f4, f5, f6])
        return inputs

    # def reset(self, y=450):
    #     self.vy = 0
    #     self.y = y
    #     self.time_alive = 0
    #     self.is_alive = True

    # set weights to zero
    # if np.random.random() < mutation_prob/2:
    #    mutation_mask = np.random.random(array.shape) < 0.02
    #    array[mutation_mask] = 0
