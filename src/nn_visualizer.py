"""
Some horrible code for visualizing a specific net
"""
import os
import pygame
import numpy as np
from pybrain.structure import SigmoidLayer, TanhLayer
from pybrain.tools.shortcuts import buildNetwork
import matplotlib
matplotlib.use("Agg")
import matplotlib.backends.backend_agg as agg
import matplotlib.pyplot as plt


cmap1 = plt.get_cmap('Reds')
cmap2 = plt.get_cmap('Greens')


def get_color(w, max_val=255, cap=1):
    v = min(abs(w), cap) / cap
    v *= 0.95  # edges of cmap too dark
    if w < 0:
        return cmap1(v)
    else:
        return cmap2(v)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class NNVisualizer:
    """
    Assumes the 6, 4, 4, 1 layout since pybrain is annoying :)
    """

    layout = (6, 4, 4, 1)

    def __init__(self, net):
        assert len(net.params) == 53
        self.net = net

        # print network info
        if False:
            for mod in self.net.modulesSorted:
                print("Module:", mod.name)
                for conn in self.net.connections[mod]:
                    print("-connection to", conn.outmod.name)
                    if conn.paramdim > 0:
                        print("- parameters", conn.params)

        # get biases
        conns = self.net.connections[self.net.modulesSorted[0]]
        for c in conns:
            if 'hidden0' in str(c.outmod):
                self.b1 = c.params
            elif 'hidden1' in str(c.outmod):
                self.b2 = c.params
            elif 'out' in str(c.outmod):
                self.b3 = c.params
            else:
                raise

        # get weights
        con1 = net.connections[net.modulesSorted[1]][0]
        self.w11 = con1.params.reshape(4, 6)

        con2 = net.connections[net.modulesSorted[2]][0]
        self.w22 = con2.params.reshape(4, 4)

        con3 = net.connections[net.modulesSorted[3]][0]
        self.w33 = con3.params.reshape(1, 4)

        # check
        self.sanity_check()

    def activate(self, inputs):
        self.h1 = np.tanh(np.dot(self.w11, inputs) + self.b1)
        self.h2 = np.tanh(np.dot(self.w22, self.h1) + self.b2)
        out_val = sigmoid(np.dot(self.w33, self.h2) + self.b3)[0]
        return out_val

    def sanity_check(self):
        for _ in range(10):
            inputs = np.random.normal(0, 1, (6, ))
            out1 = self.activate(inputs)
            out2 = self.net.activate(inputs)
            assert abs(out1-out2) < 1e-10, abs(out1-out2)

    def draw(self, screen, location, inputs):
        out_val = self.activate(inputs)

        # draw params
        r = 28
        dx = 132
        dy = 2 * r + 20
        lw = 1.8

        # inputs
        x = 100
        y = 100
        yc = y + 5 * dy / 2  # center y for network
        input_neurons = []
        for i in range(self.layout[0]):
            yi = y + i * dy
            input_neurons.append((x, yi))

        # first hidden
        y += dy
        x += dx
        hidden_neurons1 = []
        for i in range(self.layout[1]):
            yi = y + i * dy
            hidden_neurons1.append((x, yi))

        # second hidden
        x += dx
        hidden_neurons2 = []
        for i in range(self.layout[1]):
            yi = y + i * dy
            hidden_neurons2.append((x, yi))

        pos_out = (x+dx, yc)

        # matplotlib draw
        figsize = (6, 6)
        fig, ax = plt.subplots(figsize=figsize, dpi=100)
        fig.patch.set_alpha(0)
        ax.patch.set_alpha(0)

        # connections
        for i, p1 in enumerate(input_neurons):
            for j, p2 in enumerate(hidden_neurons1):
                w = self.w11[j, i]
                col = get_color(w, max_val=1)
                plt.plot([p1[0], p2[0]], [p1[1], p2[1]], '-', color=col, lw=lw, zorder=1)

        for i, p1 in enumerate(hidden_neurons1):
            for j, p2 in enumerate(hidden_neurons2):
                w = self.w22[j, i]
                col = get_color(w, max_val=1)
                plt.plot([p1[0], p2[0]], [p1[1], p2[1]], '-', color=col, lw=lw, zorder=1)

        p2 = pos_out
        for j, p1 in enumerate(hidden_neurons2):
            w = self.w33[0, j]
            col = get_color(w, max_val=1)
            plt.plot([p1[0], p2[0]], [p1[1], p2[1]], '-', color=col, lw=lw, zorder=1)

        # inputs
        # for nicer colors
        inputs_rescaled = [inputs[0]/2,
                           inputs[1]*2 - 1,
                           inputs[2]*2 - 2,
                           inputs[3]*2 - 3,
                           inputs[4]*5 - 5,
                           inputs[5]*5 - 5]
        for pos, val in zip(input_neurons, inputs_rescaled):
            col = get_color(val, max_val=1)
            c = plt.Circle(pos, r, facecolor=col, edgecolor='k', lw=lw, zorder=2)
            ax.add_artist(c)

        # hidden1
        for pos, val in zip(hidden_neurons1, self.h1):
            col = get_color(val, max_val=1)
            c = plt.Circle(pos, r, facecolor=col, edgecolor='k', lw=lw, zorder=2)
            ax.add_artist(c)

        # hidden2
        for pos, val in zip(hidden_neurons2, self.h2):
            col = get_color(val, max_val=1)
            c = plt.Circle(pos, r, facecolor=col, edgecolor='k', lw=lw, zorder=2)
            ax.add_artist(c)

        # output
        col_out = get_color(2 * out_val)
        c = plt.Circle(pos_out, r, facecolor=col_out, edgecolor='k', lw=lw, zorder=2)
        ax.add_artist(c)

        # name features
        x = 20
        y = 100 - r/3
        fs = 24

        ax.text(x, y, '$v_y$', fontsize=fs)
        y += dy
        ax.text(x, y, '$y$', fontsize=fs)
        y += dy
        ax.text(x, y, '$x_1$', fontsize=fs)
        y += dy
        ax.text(x, y, '$x_2$', fontsize=fs)
        y += dy
        ax.text(x, y, '$y_1$', fontsize=fs)
        y += dy
        ax.text(x, y, '$y_2$', fontsize=fs)

        # axis and save (saving to keep transparency)
        fname = 'test.png'
        plt.xlim([0, figsize[0] * 100])
        plt.ylim([0, figsize[1] * 100])
        plt.axis('off')
        plt.savefig(fname)
        plt.close()

        # draw on screen
        surf = pygame.image.load(fname)
        screen.blit(surf, location)
        os.remove(fname)


if __name__ == '__main__':

    # make network
    layout = (6, 4, 4, 1)
    net = buildNetwork(*layout, hiddenclass=TanhLayer, outclass=SigmoidLayer)

    np.random.seed(42)
    p = np.random.normal(0, 1, net.paramdim)
    inputs = np.random.normal(0, 1, (6, ))
    net._setParameters(p)

    nnv = NNVisualizer(net)

    # test draw
    loc = (200, 300)
    size_x = 1600
    size_y = 900

    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
    screen.fill((255, 255, 255))
    nnv.draw(screen, loc, inputs)

    running = True
    while running:
        # events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                running = False

        pygame.display.flip()

    pygame.quit()
