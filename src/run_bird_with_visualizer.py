import sys
import pickle
import pygame
from flappy_birds import Engine
from nn_visualizer import NNVisualizer


if __name__ == '__main__':

    # colors
    color_easy = (200, 50, 100)
    color_medium = (40, 70, 240)
    color_hard = (212, 175, 55)
    color_dict = dict(easy=color_easy, medium=color_medium, hard=color_hard)

    # game setup
    FPS = 60
    size_x = 1600
    size_y = 900
    mode = 'hard'
    seed = 1200
    nnv_location = (-65, 0)

    # setup
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
    clock = pygame.time.Clock()

    # get bird
    fname = 'saved_birds/elites_hard_pop200.pickle'
    bird = pickle.load(open(fname, 'rb'))[0]
    nnv = NNVisualizer(bird.net)

    # setup game
    birds = [bird]
    for b in birds:
        b.reset()
    bird.color = (0, 0, 0)
    game = Engine(size_x, size_y, birds, mode=mode, seed=seed)

    # run
    frame_counter = 0
    while game.has_alive_birds:

        # events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        game.update()
        game.draw(screen)

        inputs = bird.get_input_array()
        nnv.draw(screen, nnv_location, inputs)

        pygame.display.flip()
        clock.tick(FPS)
