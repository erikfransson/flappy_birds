import sys
import pickle
import pygame
from nn_bird import NNBird
from flappy_birds import Engine


class EnsembleBird(NNBird):

    def __init__(self, birds):
        super().__init__()
        self.birds = birds

    def update(self, dt, closest_pipe):
        self.time_alive += dt

        # activate net
        self.closest_pipe = closest_pipe
        inputs = self.get_input_array()

        self.jump_votes = 0
        for bird in self.birds:
            output = bird.net.activate(inputs)
            if output > 0.5:
                self.jump_votes += 1

        # jump or not
        if self.jump_votes > len(self.birds) / 2:
            self.vy += self.jump_burst

        # equation of motion
        self.vy += self.gravity*dt
        self.y += self.vy * dt
        self.rect.y = self.y

    def draw(self, screen):
        super().draw(screen)

        # parameters
        x = 25
        y = 280
        w = 220
        h = 35
        b = 4

        # text
        font = pygame.font.SysFont('timesnewroman', 35, bold=True)
        text = font.render('Ensemble vote', False, (0, 0, 0))
        screen.blit(text, (x, y))
        y += 50

        # draw vote bar
        r1 = pygame.rect.Rect(x, y, w, h)
        w_filled = self.jump_votes/len(self.birds) * (w-2*b)
        r2 = pygame.rect.Rect(x+b, y+b, w_filled, h-2*b)

        pygame.draw.rect(screen, (0, 0, 0), r1)
        if self.jump_votes > 0:
            pygame.draw.rect(screen, (20, 240, 40), r2)
        pygame.draw.line(screen, (240, 30, 30), (x+w/2, y), (x+w/2, y+h), 2)


def draw_status(screen):
    x = 20
    y = 100
    r = 20

    # trained on info
    font = pygame.font.SysFont('timesnewroman', 35, bold=True)
    text = font.render('Trained on {}'.format(train_mode.title()), False, (0, 0, 0))
    screen.blit(text, (x, y))
    y += 55

    # bird info
    alive = sum([b.is_alive for b in birds_easy])
    total = len(birds_easy)

    center = (x+r, y+r)
    pygame.draw.circle(screen, color1, center, r, 2)
    font = pygame.font.SysFont('timesnewroman', 35, bold=True)
    text = font.render('Indiviudal birds {}/{}'.format(alive, total), False, color1)
    screen.blit(text, (x+3*r, y))
    y += 55
    center = (x+r, y+r)
    pygame.draw.circle(screen, color2, center, r, 4)
    font = pygame.font.SysFont('timesnewroman', 35, bold=True)
    text = font.render('Ensemble bird', False, (0, 0, 0))
    screen.blit(text, (x+3*r, y))


if __name__ == '__main__':

    # colors
    color1 = (255, 50, 50)
    color2 = (0, 0, 0)

    # game setup
    FPS = 60
    size_x = 1600
    size_y = 900
    train_mode = 'medium'
    mode = 'hard'
    seed = 123

    # setup
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
    clock = pygame.time.Clock()

    # indiviudal birds
    fname = 'saved_birds/elites_{}_pop200.pickle'.format(train_mode)
    birds_easy = pickle.load(open(fname, 'rb'))
    for b in birds_easy:
        b.color = color1
        b.thickness = 2

    # ensemble bird
    bird_ensemble = EnsembleBird(birds_easy)
    bird_ensemble.color = color2

    # setup game
    birds = birds_easy + [bird_ensemble]
    for b in birds:
        b.reset()
    game = Engine(size_x, size_y, birds, mode=mode, seed=seed)

    while game.has_alive_birds:

        # events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        game.update()
        game.draw(screen)
        draw_status(screen)
        pygame.display.flip()
        clock.tick(FPS)
