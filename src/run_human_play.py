import sys
import pygame
from flappy_birds import Engine, Bird


class HumanBird(Bird):

    def jump(self):
        self.vy += self.jump_burst


if __name__ == '__main__':

    # game setup
    FPS = 60
    size_x = 1600
    size_y = 900
    mode = 'hard'
    seed = None

    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
    clock = pygame.time.Clock()

    # human play

    while True:

        human_bird = HumanBird()
        game = Engine(size_x, size_y, [human_bird], mode=mode, seed=seed)

        while game.has_alive_birds:

            # events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        human_bird.jump()

            game.update()
            game.draw(screen)
            pygame.display.flip()
            clock.tick(FPS)
        print('SCORE : ', human_bird.time_alive)
