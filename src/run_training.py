import os
import pygame
from evolution_simulation import EvolutionaryFlappyBirds


# parameters
pop_size = 200
mode = 'easy'
birds_fname = 'saved_birds/elites_{}_pop{}.pickle'.format(mode, pop_size)

if os.path.isfile(birds_fname):
    raise ValueError(birds_fname)


# setup
pygame.init()
evo = EvolutionaryFlappyBirds(population_size=pop_size, mode=mode)

# run
evo.run(birds_fname=birds_fname)
