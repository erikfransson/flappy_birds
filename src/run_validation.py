import sys
import pickle
import pygame
from flappy_birds import Engine


def draw_names(screen, birds):
    # draw explanation of colors
    r = 20
    t = 4
    x = 20
    y = 120
    font = pygame.font.SysFont('timesnewroman', 40, bold=True)
    for bird in birds:
        name = bird.name
        color = color_dict[name]
        center = (x+r, y+r)
        pygame.draw.circle(screen, color, center, r, t)
        s = 'Trained on ' + name.title()
        text = font.render(s, False, color)
        screen.blit(text, (x+3*r, y-3))

        # if dead
        if not bird.is_alive:
            x2, _ = text.get_size()
            y2 = y + bird.radius
            pygame.draw.line(screen, color, (x-0.5*r, y2), (x2 + 4 * r, y2), 5)

        y = y + 60


if __name__ == '__main__':

    # colors
    color_easy = (200, 50, 100)
    color_medium = (40, 70, 240)
    color_hard = (212, 175, 55)
    color_dict = dict(easy=color_easy, medium=color_medium, hard=color_hard)

    # game setup
    FPS = 60
    size_x = 1600
    size_y = 900
    mode = 'hard'
    seed = 1200

    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)
    clock = pygame.time.Clock()

    # read birds
    fname = 'saved_birds/elites_easy_pop200.pickle'
    bird_easy = pickle.load(open(fname, 'rb'))[0]
    bird_easy.name = 'easy'

    fname = 'saved_birds/elites_medium_pop200.pickle'
    bird_medium = pickle.load(open(fname, 'rb'))[0]
    bird_medium.name = 'medium'

    fname = 'saved_birds/elites_hard_pop200.pickle'
    bird_hard = pickle.load(open(fname, 'rb'))[0]
    bird_hard.name = 'hard'

    # setup game
    birds = [bird_easy, bird_medium, bird_hard]
    for b in birds:
        b.reset()
        b.color = color_dict[b.name]
    game = Engine(size_x, size_y, birds, mode=mode, seed=seed)

    while game.has_alive_birds:

        # events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        game.update()
        game.draw(screen)
        draw_names(screen, birds)

        pygame.display.flip()
        clock.tick(FPS)
